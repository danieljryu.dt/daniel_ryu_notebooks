import pandas as pd

exp_1 = [' rt','-rt','_rt',' pr','-pr','_pr',' retar','-retar','-1','- 1']
exp_2 = [' ua','-ua','-2','- 2']
exp_3 = [' gp','-gp','-3','- 3']
exp_4 = ['true gp','true-gp','true_gp','-4','- 4']
exp_5 = ['-5','- 5','vzw']
exp_6 = ['-6','- 6','samsung']
exp_1_uuid = ['33bff90d655b976e9399f1d0b0463d22','eb6dbe92b98fe9a91b00eb06ae37070e','5c2cc36dc709f65250ad09e9926f2020','d8be47c87ebd5047c552f92421103f1a','dad68c13e535c25d96b8a8af78f04033','608ffd6123bdbf7eef34f27889501862','5fef83d15c5b811b424ed58aeb82d0a7','e98366a9ffed9ac744ce7a0924fc1358']
exp_2_uuid = ['6e8c92368479165b5464224b5dc43075','a2866facee769a34a4a20a3f77bf134a','d68c7b0af30354df7c23485a57b8f17e']
exp_3_uuid = []
exp_4_uuid = ['8f6af55d3b890df1566e59c4bcf13d3c','f1d182a7fabffea0694ae7287aa26c0d','8eb360a67cbd36690c4a47b440916225']
exp_5_uuid = []
exp_6_uuid = []

def classify_campaigns(r):
    if r['ssb_buyer_id'] != 3549:
        return -1 
    elif r['ssb_campaign_uuid'] in exp_1_uuid:
        return 1
    elif r['ssb_campaign_uuid'] in exp_2_uuid:
        return 2
    elif r['ssb_campaign_uuid'] in exp_3_uuid:
        return 3
    elif r['ssb_campaign_uuid'] in exp_4_uuid:
        return 4
    elif r['ssb_campaign_uuid'] in exp_5_uuid:
        return 5
    elif r['ssb_campaign_uuid'] in exp_6_uuid:
        return 6
    elif any(substr in str(r['campaign_name']).lower() for substr in exp_1):
        return 1
    elif any(substr in str(r['campaign_name']).lower() for substr in exp_4):
        return 4
    elif 'pandora' in str(r['campaign_name']).lower() and 'ios' in str(r['campaign_name']).lower():
        return 4
    elif any(substr in str(r['campaign_name']).lower() for substr in exp_3):
        return 3
    elif any(substr in str(r['campaign_name']).lower() for substr in exp_2):
        return 2
    elif any(substr in str(r['campaign_name']).lower() for substr in exp_5):
        return 5
    elif any(substr in str(r['campaign_name']).lower() for substr in exp_6):
        return 6
    else:
        return -1


